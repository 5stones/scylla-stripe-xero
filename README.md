# Scylla-Stripe-Xero

Scylla-Stripe provides the basic utilities to integrate with the standard Stripe API.

Currently this module is responsible for pushing in Stripe service charges into Xero for each Payout that is generated.
