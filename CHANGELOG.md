# [1.1.0](https://gitlab.com/5stones/scylla-stripe-xero/compare/v1.0.1...v1.1.0) (2019-04-09)


### Features

* **ToXeroBankTransaction:** Add the ability to push through a service refund ([ba85f27](https://gitlab.com/5stones/scylla-stripe-xero/commit/ba85f27))



## [1.0.1](https://gitlab.com/5stones/scylla-stripe-xero/compare/v1.0.0...v1.0.1) (2019-03-11)


### Bug Fixes

* **ToXeroBankTransaction:** Fix issue with fee totals by simply summing up fee data ([fe49108](https://gitlab.com/5stones/scylla-stripe-xero/commit/fe49108))



# [1.0.0](https://gitlab.com/5stones/scylla-stripe-xero/compare/a2d11ab...v1.0.0) (2019-01-18)


### Features

* ***/*:** Add basic functionality for syncing batch payments from Stripe into Xero ([a2d11ab](https://gitlab.com/5stones/scylla-stripe-xero/commit/a2d11ab))
* **App, ToXeroBankTransaction:** Convert Stripe Payout's to XeroBankTransactions ([ed1f2cd](https://gitlab.com/5stones/scylla-stripe-xero/commit/ed1f2cd))



