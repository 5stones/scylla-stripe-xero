from scylla import app
from scylla import configuration
import scylla_stripe as stripe
import scylla_xero as xero
from . import conversions
from pprint import pprint

class App(app.App):

    def _prepare(self):
        self.xero_client = xero.XeroClient()

        stripe_config = configuration.getSection('Stripe')
        xero_config = configuration.getSection('Xero')

        task = xero.ToXeroTask(
            getattr(self.xero_client, 'banktransactions'),
            (stripe_config.get('name'), 'Payout'),
            conversions.ToXeroBankTransaction(
                xero_config.get('stripe').get('contact_id'),
                xero_config.get('stripe').get('bank_account'),
                xero_config.get('stripe').get('gl_account'),
            ),
            (xero_config.get('name'), 'BankTransaction'),
            where=None,
            with_reflection=False
        )

        self.tasks['service-fees'] = task
