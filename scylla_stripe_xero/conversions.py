from scylla import convert
from scylla import configuration
from scylla import orientdb
from scylla import log
from dateutil import parser
from pprint import pprint


class ToXeroBankTransaction(convert.Conversion):

    from_type = 'Payout'
    to_type = 'BatchTransaction'

    def __init__(self, contact_id, bank_account_code, gl_code):
        super(ToXeroBankTransaction, self).__init__()

        self.conversion_map.update({
            'Type': (self._get_payout_fees, lambda fees: 'SPEND' if fees > 0.0 else 'RECEIVE' ),
            'Contact': { 'ContactID': contact_id },
            'Date': ('created', self._get_datetime, ),
            'Reference': ('id', ),
            'BankAccount': { 'Code': bank_account_code },
            'LineItems': (lambda payout: self._get_line_charges(payout, gl_code), ),
            'TotalTax': '0.00',
            'Status': 'AUTHORISED',
        })

    @staticmethod
    def _get_datetime(d):
        if isinstance(d, basestring):
            return parser.parse(d)
        else:
            return d

    @staticmethod
    def _get_payout_fees(payout):
        summary = payout.get('summary')
        charge_fees = summary.get('charge_fees', 0)
        refund_fees = summary.get('refund_fees', 0)
        adjustment_fees = summary.get('adjustment_fees', 0)
        validation_fees = summary.get('validation_fees', 0)

        fees = (charge_fees + refund_fees + adjustment_fees + validation_fees) / 100.0

        return fees

    @staticmethod
    def _get_line_charges(payout, gl_code):
        fees = ToXeroBankTransaction._get_payout_fees(payout)
        description = 'Service Fees: {}'

        if fees <= 0.0:
            description = 'Service Fees (Refund): {}'

        return [{
            'Description': description.format(payout.get('description')),
            'Quantity': '1',
            'UnitAmount': abs(fees),
            'AccountCode': gl_code,
            'TaxType': 'NONE',
            'TaxAmount': '0.00',
        }]
